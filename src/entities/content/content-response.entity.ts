import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';
import { BaseFieldEntity } from '../default/base-field.entity';

@Entity('content_response')
@Index('content_response_id_UNIQUE', ['content_id', 'user_id'], {
  unique: true,
})
export class ContentResponseEntity extends BaseFieldEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'int' })
  content_id: number;

  @Column({ type: 'int' })
  user_id: number;

  @Column({ type: 'timestamp with time zone', nullable: true })
  like_at?: string;

  @Column({ type: 'timestamp with time zone', nullable: true })
  dislike_at?: string;

  @Column({ type: 'boolean', nullable: true, default: false })
  is_like?: boolean;

  @Column({ type: 'boolean', nullable: true, default: false })
  is_dislike?: boolean;
}
