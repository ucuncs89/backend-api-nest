import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BaseFieldEntity } from '../default/base-field.entity';
import { OrganizationEntity } from '../organization/organization.entity';

@Entity('contents')
export class ContentsEntity extends BaseFieldEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar' })
  title: string;

  @Column({ type: 'text' })
  description: string;

  @Column({ type: 'text' })
  image: string;

  @Column({ type: 'varchar' })
  slug: string;

  @Column({ type: 'int' })
  like_count: number;

  @Column({ type: 'int' })
  dislike_count: number;

  @Column({ type: 'int' })
  status: number;

  @ManyToMany(() => OrganizationEntity)
  @JoinTable({
    name: 'content_organization',
    joinColumn: {
      name: 'content_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'organization_id',
      referencedColumnName: 'id',
    },
  })
  organization: OrganizationEntity[];
}
