import { Module } from '@nestjs/common';
import { ContentsController } from './controllers/contents.controller';
import { ContentsService } from './services/contents.service';

@Module({
  controllers: [ContentsController],
  providers: [ContentsService],
})
export class ContentsModule {}
