import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TasksEntity } from 'src/entities/tasks/tasks.entity';
import { AppErrorException } from 'src/exceptions/app-exception';
import { In, Repository } from 'typeorm';
import { CreateTasksDTO } from '../dto/tasks.dto';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TasksEntity)
    private tasksRepository: Repository<TasksEntity>,
  ) {}

  async createTasks(createTasksDTO: CreateTasksDTO, user_id: number) {
    const tasks = this.tasksRepository.create({
      title: createTasksDTO.title,
      description: createTasksDTO.description,
      status: createTasksDTO.status,
      user_id,
      organization_id: createTasksDTO.organization_id,
    });
    await this.tasksRepository.insert(tasks);
    return tasks;
  }
  async findAll(payload) {
    const { page, page_size, status, user_id, organization } = payload;
    try {
      const result = await this.tasksRepository.find({
        where: { user_id, status, organization_id: In(organization) },
        skip: page,
        take: page_size,
      });
      const total_data = await this.tasksRepository.count({
        where: { user_id, status, organization_id: In(organization) },
      });
      return { result, total_data };
    } catch (error) {
      throw new AppErrorException({ ...error });
    }
  }
}
