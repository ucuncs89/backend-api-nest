import {
  Controller,
  Post,
  Body,
  Req,
  UseGuards,
  Get,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/modules/auth/jwt-auth.guard';
import { Role } from 'src/modules/roles/enum/role.enum';
import { HasRoles } from 'src/modules/roles/has-roles.decorator';
import { RolesGuard } from 'src/modules/roles/roles.guard';
import { Pagination } from 'src/utils/pagination';
import { CreateTasksDTO, FindListsTasksDTO } from '../dto/tasks.dto';
import { TasksService } from '../services/tasks.service';

@ApiBearerAuth()
@Controller('tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Post()
  @HasRoles(Role.USER)
  @UseGuards(JwtAuthGuard, RolesGuard)
  async createTaks(@Body() createTasksDTO: CreateTasksDTO, @Req() req) {
    const data = await this.tasksService.createTasks(
      createTasksDTO,
      req.user.id,
    );
    return { message: 'Successfuly', data };
  }

  @Get()
  @HasRoles(Role.USER, Role.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  async findAll(@Query() findListsTasksDTO: FindListsTasksDTO, @Req() req) {
    const _page = findListsTasksDTO.page || 1;
    const _page_size = findListsTasksDTO.page_size || 10;
    const result = await this.tasksService.findAll({
      page: (_page - 1) * _page_size,
      page_size: _page_size,
      status: findListsTasksDTO.status,
      user_id: findListsTasksDTO.user_id,
      organization: req.user.organization,
    });
    const pagination = await Pagination.pagination(
      result.total_data,
      _page,
      _page_size,
      `tasks`,
    );
    const data = {
      ...result,
      pagination,
    };
    return { message: 'Get Tasks List Successfully', data };
  }
}
