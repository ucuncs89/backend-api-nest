import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateTasksDTO {
  @ApiProperty()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  organization_id: number;

  @ApiProperty()
  @IsNotEmpty()
  status: string;
}

export class FindListsTasksDTO {
  @ApiProperty({ required: false })
  page?: number;

  @ApiProperty({ required: false })
  page_size?: number;

  @ApiProperty({ required: false })
  status?: string;

  @ApiProperty({ required: false })
  user_id?: number;
}
