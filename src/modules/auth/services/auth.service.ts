import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { UsersEntity } from 'src/entities/users/users.entity';
import {
  AppErrorException,
  AppErrorNotFoundException,
} from 'src/exceptions/app-exception';
import { UsersService } from 'src/modules/users/services/users.service';
import { Repository } from 'typeorm';
import { LoginUserDTO, RegisterUserDTO } from '../dto/auth.dto';
import { saltOrRounds } from '../../../constant/saltOrRounds';
import { JwtService } from '@nestjs/jwt/dist';
@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UsersEntity)
    private userRepository: Repository<UsersEntity>,
    private userService: UsersService,
    private jwtService: JwtService,
  ) {}
  async registerUser(registerUserDTO: RegisterUserDTO) {
    const findUserByEmail = await this.userService.findUserByEmail(
      registerUserDTO.email,
    );
    if (findUserByEmail) {
      throw new AppErrorException('Email Already Exists');
    }
    const findByUsername = await this.userService.findUserByUsername(
      registerUserDTO.username,
    );
    if (findByUsername) {
      throw new AppErrorException('Username Already Exists');
    }
    const passwordHash = await bcrypt.hash(
      registerUserDTO.password,
      saltOrRounds,
    );
    const insert = this.userRepository.create({
      email: registerUserDTO.email.toLowerCase(),
      full_name: registerUserDTO.full_name,
      password: passwordHash,
      username: registerUserDTO.username.toLowerCase(),
      is_active: false,
      organization: registerUserDTO.organization_id
        ? [{ id: registerUserDTO.organization_id }]
        : null,
      roles: [{ id: 1 }],
    });
    await this.userRepository.save(insert);
    return insert;
  }
  async loginUser(loginUserDTO: LoginUserDTO) {
    const findUser = await this.userRepository.findOne({
      where: [
        { email: loginUserDTO.username_email.toLowerCase() },
        { username: loginUserDTO.username_email.toLowerCase() },
      ],
      relations: { roles: true, organization: true },
    });
    if (!findUser) {
      throw new AppErrorNotFoundException('User not found');
    }
    if (!findUser.is_active) {
      throw new AppErrorException('User not active');
    }
    const compare = await bcrypt.compare(
      loginUserDTO.password,
      findUser.password,
    );
    if (!compare) {
      throw new AppErrorException('Wrong Password');
    }
    const organization = findUser.organization.map((v) => v.id);
    const roles = findUser.roles.map((v) => v.id);

    const payloadJwt = {
      id: findUser.id,
      email: findUser.email,
      full_name: findUser.full_name,
      username: findUser.username,
      roles,
      organization,
    };

    const token = this.jwtService.sign(payloadJwt);

    return { ...findUser, token };
  }
  async validateUserJWT(payload) {
    const data = await this.userRepository.findOne({
      where: { id: payload.id },
    });
    return data;
  }
}
