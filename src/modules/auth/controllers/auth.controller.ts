import { Controller, Post, Body } from '@nestjs/common';
import { LoginUserDTO, RegisterUserDTO } from '../dto/auth.dto';
import { AuthService } from '../services/auth.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  async register(@Body() registerUserDTO: RegisterUserDTO) {
    const data = await this.authService.registerUser(registerUserDTO);
    return { data };
  }
  @Post('login')
  async login(@Body() loginUserDTO: LoginUserDTO) {
    const data = await this.authService.loginUser(loginUserDTO);
    return { data };
  }
}
