import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEmail } from 'class-validator';
export class RegisterUserDTO {
  @ApiProperty()
  @IsNotEmpty()
  username: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @ApiProperty()
  password: string;

  @ApiProperty()
  @IsNotEmpty()
  full_name: string;

  @ApiProperty()
  organization_id?: number;
}
export class LoginUserDTO {
  @ApiProperty()
  @IsNotEmpty()
  username_email: string;

  @ApiProperty()
  @IsNotEmpty()
  password: string;
}
