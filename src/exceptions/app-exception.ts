import { BadRequestException, NotFoundException } from '@nestjs/common';
import { UnauthorizedException } from '@nestjs/common/exceptions';
export class AppErrorException extends BadRequestException {
  constructor(message?: string) {
    super({
      message: message || 'error',
      error_code: 'err',
    });
  }
}

export class AppErrorNotFoundException extends NotFoundException {
  constructor(message?: string) {
    super({
      message: message || 'error not found',
      error_code: 'err-not-found',
    });
  }
}

export class AppUnauthorizedException extends UnauthorizedException {
  constructor(message?: string) {
    super({
      message: message || 'Unauthorized',
      error_code: 'err',
    });
  }
}
